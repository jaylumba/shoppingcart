package com.example.ongkr.shoppingcart;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class SampleMainActivity extends FragmentActivity {
    private static final String TAG = "LOG::";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_two);
    }
}
