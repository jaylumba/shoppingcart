package com.example.ongkr.shoppingcart;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class GridviewAdapter extends BaseAdapter {

    OneFragment context;

    ArrayList<Beanclass> bean;
    OneFragment main;

    public GridviewAdapter(OneFragment activity, OneFragment context, ArrayList<Beanclass> bean) {
        this.main = activity;
        this.bean = bean;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.grid_productlist, null);

            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);

            viewHolder.image = convertView.findViewById(R.id.product_image);
            viewHolder.title = convertView.findViewById(R.id.product_title);
            viewHolder.price = convertView.findViewById(R.id.product_price);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Beanclass bean = (Beanclass) getItem(position);

        viewHolder.image.setImageResource(bean.getImage());
        viewHolder.title.setText((CharSequence) bean.getTitle());
        viewHolder.price.setText(bean.getPrice());

        return convertView;
    }

    private class ViewHolder {
        ImageView image;
        TextView title;
        TextView price;
    }
}







