package com.example.ongkr.shoppingcart;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class OneFragment extends Fragment {

    private ExpandableHeightGridView gridview;
    private ArrayList<Beanclass> beanclassArrayList;
    private GridviewAdapter gridviewAdapter;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View RootView = inflater.inflate(R.layout.fragment_one, container, false);
        gridview = RootView.findViewById(R.id.gridviewlayout);

        beanclassArrayList= new ArrayList<Beanclass>();

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String LINK = "http://192.168.0.100:2000/product";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, LINK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            // Display the first 500 characters of the response string.

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jo = jsonArray.getJSONObject(i);
                                // Do you fancy stuff
                                Log.d(TAG,"onResponse() returned: " + jo.getString("name"));
                                Beanclass beanclass = new Beanclass(R.drawable.shoppy_logo,jo.getString("name"), jo.getString("price"));
                                beanclassArrayList.add(beanclass);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"onResponse() returned: " + error);
            }
        });
        Log.d(TAG,"data to be positioned on grids: " + beanclassArrayList);
        gridviewAdapter = new GridviewAdapter(this,OneFragment.this, beanclassArrayList);

        gridview.setAdapter(gridviewAdapter);

        gridview.setExpanded(true);

        queue.add(stringRequest);

        return RootView;
    }
}
